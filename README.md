# Documentation

This is the documentation of the hosting project.

## Build

```shell
# Install dependencies
bundle install

# Run development server
bundle exec middleman server

# Build
bundle exec middleman build
```
