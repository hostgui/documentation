# Hooks

## domain
> Example

```toml
[[hook]]
operation = "domain.delete"
action = "post"
execute = "/usr/bin/systemctl reload nginx"
required = true
```

Contains hooks on operations concerning web hosts.

### Execution Parameters

- *name*: The domain name.
- *root*: The root of the virtual host.

## account
> Example

```toml
operation = "account.update"
action = "post"
execute = "/etc/hosting/account-update.sh --username {{username}}"
```

### Execution Parameters

- *username*: The account username.
- *domain*: The domain the account belongs to.

Contains hooks on operations concerning upload accounts.
