# API Reference
Using this API, CRUD operations as well as hooks potentially configured in the configuration file may be executed on the database.

**Base URL:** /api

**Auth:** All request have to be authenticated using an API key provided in the `X-API-Key` header

## **GET** /domain

> Example response

```json
[
  {
    "name": "example.org",
    "quota": 512,
    "create_time": "201901271608",
    "update_time": "201901271608"
  },
  {
    "name": "test.com",
    "quota": 1024,
    "create_time": "201901271608",
    "update_time": "201901271608"
  }
]
```

Provides a list of all domains available for configuration.

**Response:**

- *name*: The domain name.
- *quota*: The maximum storage space the webspace of this domain might use.
- *create_time*: The timestamp of the domain creation.
- *update_time*: The timestamp of the last update of the domain.

## **POST** /domain
> Example request

```json
{
  "name": "foo.com",
  "quota": 512
}
```

> Response (`200`)

```json
{ "message": "ok" }
```

Add a new domain.

**Request:**

- *name:* The domain.
- *quota*: The maximum storage space the webspace of this domain might use.

## **POST** /domain/{domain}
> Example request

```json
{
   "name": "bar.org"
}
```

> Response (`200`)

```json
{ "message": "ok" }
```

Updates a domain.

- *domain:* The domain.

**Request:**

- *name: (optional):* The new domain name.
- *quota: (optional):* The new quota of the domain.

## **DELETE** /domain/{domain}
> Response (`200`)

```json
{ "message": "ok" }
```

Deletes the specified domain.

- *domain:* The domain.

## **GET** /upload/{domain}
> Example response

```json
[
  {
    "username": "test",
    "create_time": "201901291503",
    "update_time": "201901291503"
  }
]
```

> Response (`200`)

```json
{ "message": "ok" }
```

Provides a list of accounts on the specified domain name.

- *domain:* The domain.

**Response:**

- *username*: The account username.
- *access_mail*: Whether the account has access to mails or not.
- *receive_mail*: Whether the account can receive emails or not.
- *create_time*: The timestamp of the account creation
- *update_time*: The timestamp of the last update of the account
- *quota*: The maximum storage space the account might use.

## **POST** /upload/{domain}
> Example request

```json
{
  "username": "test",
  "password": "not12345"
}
```

> Response (`200`)

```json
{ "message": "ok" }
```

Adds a new account to the domain.

- *domain:* The domain.

**Request**:

- *username*: The account username.
- *password*: The password of the account.

## **POST** /upload/{domain}/{user}
> Example request

```json
{
  "username": "test"
}
```

> Response (`200`)

```json
{ "message": "ok" }
```

Updates account information.

- *domain*: The domain.
- *user*: The account username.

**Request**:

- *username*: The new account username.
- *password*: The new password of the account.

## **DELETE** /upload/{domain}/{user}
> Response (`200`)

```json
{ "message": "ok" }
```

Deletes the specified account.

- *domain*: The domain.
- *user*: The account username.
