# Configuration
Configuration files are written using the [TOML](https://github.com/toml-lang/toml) standard. One separate configuration file is dedicated to each module, however global options might be provided in a separate file and then linked in the module configs.

In the following, option descriptions will look as follows:

- *optional*: This option is not required. *Default: `defaultvalue`*
- **required**: This option must be provided under all circumstances.
- **maybe_required** [key = `somevalue`]: This option is required if the conditions in square brackets are met, otherwise ignored.

## Webserver Options
```toml
[webserver]
configfile = "/etc/nginx/conf.d/{{hostname}}.conf"
template = "/etc/hosting/virtualhost.conf.tpl"
root = "/var/www/{{hostname}}"
```

The `webserver` group provides all information needed to generate proper configuration

- **configfile**: The file the webserver stores its configuration files in.
- **template**: A template file for the generated configuration files.
- *root*: The root of files created. *Defaults to `/var/www/{{hostname}}`.*

### Variables
Values containing `{{variables}}` are replaced according to the respective virtual host.

- *hostname*: The hostname of the virtual host.

# Configuration File Templates
> Example template
```
server {
        listen 80;
        listen [::]:80;

        root {{root}};
        index index.html;

        server_name {{hostname}};
}
```

In order to support any kind of web server, configuration files are generated using a template. `{{variables}}` in this file are simply replaced by the actual value and written.

### Variables

- *root*: The root directory 
- *hostname*: The hostname of the virtual host.
