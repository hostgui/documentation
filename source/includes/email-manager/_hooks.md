# Hooks

## account
> Example

```toml
[[hook]]
operation = "account.create"
action = "pre"
execute = "/etc/hosting/hooks/pre-save.sh --local {{local}} --domain {{domain}}"
required = true
```

Contains hooks on operations concerning email accounts.

### Execution Parameters

#### CREATE

- username
- access_mail
- receive_mail
- domain
- create_time
- update_time
- quota

#### UPDATE

- username
- access_mail
- receive_mail
- domain
- create_time
- update_time
- quota

#### DELETE

- username
- domain

## domain
> Example

```toml
[[hook]]
operation = "domain.create"
action = "post"
execute = "/etc/hosting/create-domain.sh --domain {{domain}}"
```

Contains hooks on operations concerning email domains.

### Execution Parameters

#### CREATE

- name
- quota

#### UPDATE

- name
- quota

#### DELETE

- name

## alias
> Example

```toml
[[hook]]
operation = "alias.create"
action = "pre"
execute = "/etc/hosting/create-alias.sh --alias {{alias}}"
```

Contains hooks on operations concerning email aliases.

### Execution Parameters

#### CREATE

- domain
- account
- alias

#### UPDATE

- domain
- account
- alias

#### DELETE

- domain
- account
- alias

## forward
> Example

```toml
[[hook]]
operation = "forward.create"
action = "pre"
execute = "/etc/hosting/create-forward.sh --forward {{forward}}"
```

Contains hooks on operations concerning email forwards.

### Execution Parameters

#### CREATE

- domain
- account
- forward

#### UPDATE

- domain
- account
- forward

#### DELETE

- domain
- account
- forward
