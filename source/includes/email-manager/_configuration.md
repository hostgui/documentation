# Configuration

```toml
maildir = "/var/lib/mail"
```

- **maildir**: The directory received mail is located in.
