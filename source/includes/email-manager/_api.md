# API Reference
Using this API, CRUD operations as well as hooks potentially configured in the configuration file may be executed on the database.

**Base URL:** /api

**Auth:** All request have to be authenticated using an API key provided in the `X-API-Key` header

## **GET** /domain

> Example response

```json
[
  { "id": 0, "name": "example.org", "quota": 512 },
  { "id": 1, "name": "test.com", "quota": 1024 }
]
```

Provides a list of all domains available for configuration.

**Response:**

- *id*: The ID of the domain in the database.
- *name*: The domain name.
- *quota*: The maximum storage space email accounts on this domain might use.

## **POST** /domain
> Example request

```json
{
  "name": "foo.com",
  "quota": 512
}
```

> Response (`200`)

```json
{ "message": "ok" }
```

Add a new domain.

**Request:**

- *name:* The domain.
- *quota (optional):* The maximum storage space email accounts on this domain might use.

## **POST** /domain/{domain}
> Example request

```json
{
   "name": "bar.org"
}
```

> Response (`200`)

```json
{ "message": "ok" }
```

Updates a domain.

- *domain:* The domain.

**Request:**

- *name: (optional):* The new domain name.
- *quota: (optional):* The new quota of the domain.

## **DELETE** /domain/{domain}
> Response (`200`)

```json
{ "message": "ok" }
```

Deletes the specified domain.

- *domain:* The domain.

## **GET** /account/{domain}
> Example response

```json
[
  {
    "username": "test",
    "access_mail": true,
    "receive_mail": false,
    "create_time": "201901291503",
    "update_time": "201901291503",
    "quota": 512
  }
]
```

> Response (`200`)

```json
{ "message": "ok" }
```

Provides a list of accounts on the specified domain name.

- *domain:* The domain.

**Response:**

- *username*: The account username.
- *access_mail*: Whether the account has access to mails or not.
- *receive_mail*: Whether the account can receive emails or not.
- *create_time*: The timestamp of the account creation.
- *update_time*: The timestamp of the last update of the account.
- *quota*: The maximum storage space the account might use.

## **POST** /account/{domain}
> Example request

```json
{
  "username": "test",
  "access_mail": true,
  "receive_mail": false,
  "password": "not12345",
  "quota": 512
}
```

> Response (`200`)

```json
{ "message": "ok" }
```

Adds a new account to the domain.

- *domain:* The domain.

**Request**:

- *username*: The account username.
- *access_mail*: Whether the account has access to mails or not.
- *receive_mail*: Whether the account can receive emails or not.
- *password*: The password of the account.
- *quota*: The maximum storage space the account might use.

## **POST** /account/{domain}/{user}
> Example response

```json
{
  "username": "test",
  "access_mail": true,
  "receive_mail": false,
  "quota": 512
}
```

Updates account information.

- *domain*: The domain.
- *user*: The account username.

**Request**:

- *username*: The account username.
- *access_mail*: Whether the account has access to mails or not.
- *receive_mail*: Whether the account can receive emails or not.
- *password*: The password of the account.
- *quota*: The maximum storage space the account might use.

## **DELETE** /account/{domain}/{user}
> Response (`200`)

```json
{ "message": "ok" }
```

Deletes the specified account.

- *domain*: The domain.
- *user*: The account username.

## **GET** /alias/{domain}/{user}
> Example response

```json
[
  "foo",
  "bar"
]
```

Provides a list of aliases for an email account.

- *domain*: The domain.
- *user*: The account username.

## **POST** /alias/{domain}/{user}
> Example Request

```json
{ "alias": "something" }
```

> Response (`200`)

```json
{ "message": "ok" }
```

Adds a new alias for an email account.

- *domain*: The domain.
- *user*: The account username.

## **DELETE** /alias/{domain}/{user}/{alias}
> Response (`200`)

```json
{ "message": "ok" }
```

Deletes an alias for an email account.

- *domain*: The domain.
- *user*: The account username.
- *alias*: The alias that should be deleted.

## **GET** /forward/{domain}/{user}
> Example response

```json
[
  "foo@gmail.com",
  "bar@hotmail.com"
]
```

Provides a list of forwards for an email account.

- *domain*: The domain.
- *user*: The account username.

## **POST** /forward/{domain}/{user}
> Example Request

```json
{ "forward": "bar@gmail.com" }
```

> Response (`200`)

```json
{ "message": "ok" }
```

Adds a new forward for an email account.

- *domain*: The domain.
- *user*: The account username.

## **DELETE** /forward/{domain}/{user}/{forward}
> Response (`200`)

```json
{ "message": "ok" }
```

Deletes an forward for an email account.

- *domain*: The domain.
- *user*: The account username.
- *forward*: The forward that should be deleted.
