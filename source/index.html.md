---
title: Hosting Documentation

toc_footers:
  - <a href="email-manager.html">Email Manager</a>
  - <a href="web-manager.html"><b>Web Manager</b></a>
  - <a href="admin-panel.html">Administrator Panel</a>

search: true
---

# Introduction
This software allows for an easy administration of servers hosting email accounts and/or web services. It is written entirely in PHP and divided into three modules (the [email manager](#email-manager), the [web manager](#web-manager) and the [administration panel](#admin-panel)) which can be reached through a RESTful API. These modules are completely separated and able to each run on different servers, securely exposing a public API.

For the sake of simplicity, hereinafter the modules are abbreviated as follows:

- EM: email manager module
- WM: web manager module
- AP: administration panel module

They all have access to the same SQL database, only manipulating tables, that are actively part of their area of responsibility. For instance, to create a new email account on a domain in the administration panel, the AP will not both send a request to the EM and create the user in the database accordingly, rather it will send a request to the EM, which in turn runs potential pre- and post-hooks as well as creates the user in the database accordingly.


<aside class="warning">Most of the following documentation is only a rough idea yet and should only be implemented after extensive elaboration.</aside>
  
# Configuration
Configuration files are written using the [TOML](https://github.com/toml-lang/toml) standard. One separate configuration file is dedicated to each module, however global options might be provided in a separate file and then linked in the module configs.

In the following, option descriptions will look as follows:

- *optional*: This option is not required. *Default: `defaultvalue`*
- **required**: This option must be provided under all circumstances.
- **maybe_required** [key = `somevalue`]: This option is required if the conditions in square brackets are met, otherwise ignored.

The following options are global and configured on each of the modules in the same way.

## Includes
```toml
[[include]]
file = "/etc/hosting/somefile.toml
```

The `include` table array can be used to include other files. This is especially useful to maintain only one file to configure the database connection, for instance, and include that file in all of the modules' configuration files.

There might be multiple includes provided. Also, the included configuration files don't differ from the actual configuration files, they are configured exactly the same way the parent file is configured.

- **file**: The path of the file that should be included.

<aside class="notice">Options provided in the parent configuration file always override values set in the included file.</aside>

## Database Options
```toml
[database]
driver = "mysql"
host = "127.0.0.1"
port = 3306
username = "notroot"
password = "not1234"
``` 

The `Database` group provides all information needed to connect to the database containing the data needed for the module.

- *driver*: The database driver used. Allowed values are `mysql`, `sqlite` and `postgresql`. *Default: `mysql`*
- *host*: The hostname of the server the database runs on. *Default: `127.0.0.1`*
- *port*: The port the database is listening on. *Default: `3306`*
- **username** [driver != `sqlite`]: The username used for authenticating to the database.
- **password** [driver != `sqlite`]`: The password used for authenticating to the database.
- **file** [driver = `sqlite`]: If a flat file database is used, this provides the path of the file.

## Hook Options
```toml
# email-manager.toml
[[hook]]
operation = "account.create"
action = "pre"
execute = "/etc/hosting/hooks/pre-save.sh --username {{username}} --domain {{domain}}"
required = true

# web-manager.toml
[[hook]]
operation = "domain.create"
action = "post"
execute = "/usr/bin/systemctl reload nginx"
```

Elements of the `hook` table array can be used to configure a very powerful hooking system. They can add custom actions before and after CUD operations. This might for instance be the deletion of user data after an email account was deleted.

- **operation**: The resource as well as a CUD operation on that resource that should be hooked, concatenated with a dot.
- **action**: Whether the hook should be executed before or after the operation is executed. Allowed values are `pre` and `post`.
- **execute**: The command that should be executed by the hook. HTTP/s requests can also be executed using a `wget`, `curl` or similar command.
- *required*: Whether the operation must not be executed in case of a non-zero exit code of the executed command. *Default: false*

The `execute` option is a template in which all `{{variables}}` are replaced with the respective variables provided by the operation. In case the variable name is invalid, the variable name as well as the curly braces.

<aside class="notice">Detailed information about what hooks can be used as well as available variables in the `execute` option can be found in the respective module documentation.</aside>

# Database

An SQL database is used to store the information needed for the mailserver and webserver as well as the EM, WM and AP.

The database schema is defined as seen in the following ER diagram:

<img alt="Database Model" src="images/database-model.svg" width="100%" />
*([Download](files/database-model.mwb))*

## Mailserver

**email_account**: Email accounts

| Name | Type | Description |
| --- | --- | --- |
| id | INT | ID of the account |
| domain_id | INT | Reference to a domain *(email_domain.id)* |
| username | VARCHAR(64) | Username of the account |
| password | VARCHAR(255) | Hashed password of the account |
| access_mail | BOOLEAN | Whether the account has access to emails |
| receive_mail | BOOLEAN | Whether the account can receive emails |
| create_time | TIMESTAMP | The creation time of the account |
| update_time | TIMESTAMP | The last update of account information |
| quota | INT | The email storage limit of the account |

<br/>

**email_domain**: Domains that can receive emails

| Name | Type | Description |
| --- | --- | --- |
| id | INT | ID of the domain |
| name | VARCHAR(255) | The domain name |
| quota | INT | The storage limit of the domain |

<br/>

**email_alias**: Account aliases on an email address

| Name | Type | Description |
| --- | --- | --- |
| email | VARCHAR(255) | The alias of the account |
| account_id | INT | Reference to an account *(email_account.id)* |

<br/>

**email_forward**: Email addresses to forward ingoing emails of an account to

| Name | Type | Description |
| --- | --- | --- |
| forward_address | VARCHAR(255) | The email address to forward emails to |
| account_id | INT | Reference to an account *(email_account.id)* |

## Webserver

**www_domain**: Virtual hosts of the webserver

| Name | Type | Description |
| --- | --- | --- |
| id | INT | ID of the virtual host |
| name | VARCHAR(255) | Hostname |
| quota | INT | Storage limit of uploaded data to the webserver |
| create_time | TIMESTAMP | The creation time of the host |
| update_time | TIMESTAMP | The last update of the host |

<br/>

**www_upload**: FTP accounts that have access to the host

| Name | Type | Description |
| --- | --- | --- |
| id | INT | ID of the account |
| username | VARCHAR(255) | Account username |
| domain_id | INT | Reference to a virtual host *(www_domain.id)* |
| password | VARCHAR(255) | Hashed password of the account |
| create_time | TIMESTAMP | The creation time of the account |
| update_time | TIMESTAMP | The last update of the account |

## Administration panel

**domain_admin**: Administrator accounts that can configure domains

| Name | Type | Description |
| --- | --- | --- |
| id | INT | ID of the account |
| name | VARCHAR(255) | Full name of the user |
| username | VARCHAR(64) | Username of the account |
| password | VARCHAR(255) | Hashed password of the account |
| enabled | BOOLEAN | Whether the account is enabled or not |
| create_time | TIMESTAMP | The creation time of the account |
| update_time | TIMESTAMP | The last update of the account |

<br/>

**email_domain_has_domain_admin**: Reference table to link administrator accounts to email domains

| Name | Type | Description |
| --- | --- | --- |
| email_domain_id | INT | Reference to an email domain *(email_domain.id)* |
| domain_admin_id | INT | Reference to a domain admin *(domain_admin.id)* |

<br/>

**domain_admin_has_www_domain**: Reference table to link administrator accounts to web domains

| Name | Type | Description |
| --- | --- | --- |
| www_domain_id | INT | Reference to an web domain *(www_domain.id)* |
| domain_admin_id | INT | Reference to an email domain *(email_domain.id)* |

# Modules

## Email Manager

**[Documentation](email-manager.html)**

This module exposes a REST API which allows for programmatic configuration of email domains as well as accounts.

It can run hooks before and after certain actions, such as removing stored data from the disk after the deletion of an account. This ensures a seamless integration of the AP with the mailserver.

## Web Manager

**[Documentation](web-manager.html)**

This module exposes a REST API which allows for programmatic configuration of webserver hosts as well as FTP accounts, that are allowed to upload data to them.

It can run hooks before and after certain actions, such as removing stored data from the disk after the deletion of a host. This ensures a seamless integration of the administrator panel with the webserver.

## Administration Panel

**[Documentation](admin-panel.html)**
























