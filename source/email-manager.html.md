---
title: Email Manager Documentation

includes:
  - email-manager/configuration
  - email-manager/hooks
  - email-manager/api

toc_footers:
  - <a href="index.html"><b>← Back to main</b></a>
  - <a href="web-manager.html">Web Manager</a>
  - <a href="admin-panel.html">Administrator Panel</a>

search: true
---

# Introduction

This module exposes a REST API which allows for programmatic configuration of email domains as well as accounts.

It can run hooks before and after certain actions, such as removing stored data from the disk after the deletion of an account. This ensures a seamless integration of the administrator panel with the mailserver.
