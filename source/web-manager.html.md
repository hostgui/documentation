---
title: Web Manager Documentation

includes:
  - web-manager/configuration
  - web-manager/hooks
  - web-manager/api

toc_footers:
  - <a href="index.html"><b>← Back to main</b></a>
  - <a href="email-manager.html">Email Manager</a>
  - <a href="admin-panel.html">Administrator Panel</a>

search: true
---

# Introduction

This module exposes a REST API which allows for programmatic configuration of webserver hosts as well as FTP accounts, that are allowed to upload data to them.

It can run hooks before and after certain actions, such as removing stored data from the disk after the deletion of a host. This ensures a seamless integration of the administrator panel with the webserver.
